﻿//
//  main.cpp
//  Battle Ship
//
//  Created by sebastian martinez  and Jorge Garcia on 4/9/19.
//  Copyright � 2019 sebastian martinez. All rights reserved.
//

#include <iostream>
#include <vector>
#include <string>

using namespace std;

static vector <string> column{ " ", "A", "B", "C","D", "E", "F", "G", "H", "I", "J" };
bool game_won = false;

/***************************** PLAYER ***********************************/

class Player {
public:
	Player();
	void print_grid();
	void set_player_name(string player_in);
	void set_opponent_name(string opponent_in);
	string get_opponent_name();

	string get_player_name();

	vector <vector <string>> player_grid;
	vector <vector <string>> opponent_grid;

	int height, width;

	string empty_slot;
	string player_name;
	string opponent_name;
};

Player::Player() : height(11), width(11), empty_slot("-") {

	player_grid.resize(height, vector <string>(width, "-"));
	opponent_grid.resize(height, vector <string>(width, "-"));
}

void Player::print_grid()
{
	cout << "\t YOUR BOARD \t\t\t\t\t OPPONENT BOARD \n";

	for (int i = 0; i < player_grid.size(); i++)
	{
		cout << column[i] << " "; //prints letters
		for (int j = 0; j < player_grid.size(); j++)
		{
			if (j > 0 && i == 0) // works prints numbers
				cout << j << " ";
			else if (j > 0 && i > 0) // prints -
				cout << player_grid[i][j] << " ";
		}
		cout << "\t\t\t";
		cout << column[i] << " "; //prints letters
		for (int j = 0; j < opponent_grid.size(); j++)
		{
			if (j > 0 && i == 0) // works prints numbers
				cout << j << " ";
			else if (j > 0 && i > 0) // prints -
				cout << opponent_grid[i][j] << " ";
		}
		cout << "\n";
	}
}

void Player::set_player_name(string player_in)
{
	player_name = player_in;
}

string Player::get_player_name()
{
	return player_name;
}
void Player::set_opponent_name(string opponent_in)
{
	opponent_name = opponent_in;
}

string Player::get_opponent_name()
{
	return opponent_name;
}

/***************************** BATTLE SHIP ***********************************/

class Battle_Ships {
public:
	int get_ship_size();
	int get_hit();
	int get_position_x();
	int get_position_y();
	int get_ship_id();

	bool get_sunk();

	string get_direction();
	string get_name();

	void set_name(string name_in);
	void set_sunk(bool sunk_in);
	void set_ship_size(int size_in);
	void set_position(int pos_x, int pos_y);
	void set_hit();
	void set_direction(string direction_in);
	vector <Battle_Ships> player_ships;
	vector <Battle_Ships> opponent_ships;

protected:
	int hit;
	int ship_id;
	int ship_size;
	int x, y;

	bool sunk;

	string ship_name;
	string direction;
};

bool Battle_Ships::get_sunk()
{
	return sunk;
}

void Battle_Ships::set_sunk(bool sunk_in)
{
	sunk = sunk_in;
}

int Battle_Ships::get_position_x()
{
	return x;
}

int Battle_Ships::get_position_y()
{
	return y;
}

void Battle_Ships::set_position(int pos_x, int pos_y)
{
	x = pos_x;
	y = pos_y;
}

int Battle_Ships::get_ship_size()
{
	return ship_size;
}

int Battle_Ships::get_hit()
{
	return hit;
}

int Battle_Ships::get_ship_id()
{
	return ship_id;
}

void  Battle_Ships::set_hit()
{
	hit++;
}

void Battle_Ships::set_direction(string direction_in)
{
	direction = direction_in;
}

string Battle_Ships::get_direction()
{
	return direction;
}

string Battle_Ships::get_name()
{
	return ship_name;
}

void Battle_Ships::set_name(string name_in)
{
	ship_name = name_in;
}

/***************************** BATTLE SHIPS ***********************************/

class Two_Sized_Ship : public Battle_Ships {
public:
	Two_Sized_Ship()
	{
		ship_size = 2;
		ship_name = "Ship Two";
		ship_id = 2;
		hit = 0;
		sunk = false;
	}
};

class Three_Sized_Ship : public Battle_Ships {
public:
	Three_Sized_Ship()
	{
		ship_size = 3;
		ship_name = "Ship Three";
		ship_id = 3;
		hit = 0;
		sunk = false;
	}
};

class Four_Sized_Ship : public Battle_Ships {
public:
	Four_Sized_Ship()
	{
		ship_size = 4;
		ship_name = "Ship Four";
		ship_id = 4;
		hit = 0;
		sunk = false;
	}
};

class Five_Sized_Ship : public Battle_Ships {
public:
	Five_Sized_Ship()
	{
		ship_size = 5;
		ship_name = "Ship Five";
		ship_id = 5;
		hit = 0;
		sunk = false;
	}
};

/***************************** HELPERS ***********************************/

bool validate_position(Player &p, string input, Battle_Ships &ship) // valid input within the grid
{
	for (int i = 1; i < column.size(); i++)
	{
		for (int j = 1; j < column.size(); j++)
		{
			if (input == (column[j] + to_string(i)))
			{ // ex: a9
				if (ship.get_direction() == "H")
				{
					for (int k = 0; k < ship.get_ship_size(); k++)
					{
						if ((i + k) < 11 && p.player_grid[j][i + k] != "-")
						{
							return false;
						}
					}
					if (ship.get_ship_size() + (i - 1) < 11)
					{
						ship.set_position(j, i);
						return true;
					}
				}
				else if (ship.get_direction() == "V")
				{
					for (int k = 0; k < ship.get_ship_size(); k++)
					{
						if ((j + k) < 11 && p.player_grid[j + k][i] != "-")
						{
							return false;
						}
					}
					if (ship.get_ship_size() + (j - 1) < 11)
					{
						ship.set_position(j, i);
						return true;
					}
				}
			}
			else if (input == (to_string(i) + column[j]))
			{ // ex: 9a
				if (ship.get_direction() == "H")
				{
					for (int k = 0; k < ship.get_ship_size(); k++)
					{
						if ((i + k) < 11 && p.player_grid[j][i + k] != "-")
						{
							return false;
						}
					}

					if (ship.get_ship_size() + (i - 1) < 11)
					{
						ship.set_position(j, i);
						return true;
					}
				}
				else if (ship.get_direction() == "V")
				{
					for (int k = 0; k < ship.get_ship_size(); k++)
					{
						if ((j + k) < 11 && p.player_grid[j + k][i] != "-")
						{
							return false;
						}
					}
					if (ship.get_ship_size() + (j - 1) < 11)
					{
						ship.set_position(j, i);
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool validate_direction(string direction, Battle_Ships &ship)
{
	if (direction == "V" || direction == "H")
	{
		ship.set_direction(direction);
		return true;
	}
	return false;
}

void enter_position(Player &p, Battle_Ships &ship)
{
	string position;
	bool is_valid;

	do {
		cout << "Enter position for " << ship.get_name() << " " << "(ex: row column): ";
		cin >> position;

		for (auto & c : position) c = toupper(c);

		is_valid = validate_position(p, position, ship);

		if (!is_valid)
		{
			cout << "Error: re-enter a valid input." << endl;
		}

	} while (!is_valid);
}

void enter_direction(Battle_Ships &ship)
{
	string direction;
	bool is_valid;

	do {
		cout << "Enter direction for " << ship.get_name() << " " << "(ex: H or V): ";
		cin >> direction;

		for (auto & c : direction) c = toupper(c);

		is_valid = validate_direction(direction, ship);

		if (!is_valid)
		{
			cout << "Error: re-enter a valid input." << endl;
		}

	} while (!is_valid);
}

void update_grid(Player &p, Battle_Ships &ship)
{
	int x_pos = ship.get_position_x();
	int y_pos = ship.get_position_y();

	p.player_grid[x_pos][y_pos] = to_string(ship.get_ship_id());

	if (ship.get_direction() == "V")
	{
		for (int i = 0; i < ship.get_ship_size() - 1; i++)
		{
			x_pos++;
			p.player_grid[x_pos][y_pos] = to_string(ship.get_ship_id());
		}
	}
	else
	{
		for (int i = 0; i < ship.get_ship_size() - 1; i++)
		{
			y_pos++;
			p.player_grid[x_pos][y_pos] = to_string(ship.get_ship_id());
		}
	}
}

bool validate_attack(string position, Player &my_board, Player &opponent_board)
{
	for (auto & c : position) c = toupper(c);

	for (int i = 1; i < column.size(); i++)
	{
		for (int j = 1; j < column.size(); j++)
		{

			if (position == (column[j] + to_string(i)))
			{ // ex: a9
				if (my_board.opponent_grid[j][i] == "*" || my_board.opponent_grid[j][i] == "H" || my_board.opponent_grid[j][i] == "S")
				{
					cout << " Bad move, you REPEATED a Move! " << endl;
				}
				else if (opponent_board.player_grid[j][i] == "-")
				{
					//miss and update grid
					my_board.opponent_grid[j][i] = "*";
					cout << " Bad move, you MISSED!! " << endl;

				}
				else
				{
					cout << " Attack was SUCCESSFUL!!!" << endl;
					my_board.opponent_grid[j][i] = "H";
					opponent_board.player_grid[j][i] = "X";
					return true;
				}
			}
			else if (position == (to_string(i) + column[j]))
			{
				if (my_board.opponent_grid[j][i] == "*" || my_board.opponent_grid[j][i] == "H" || my_board.opponent_grid[j][i] == "S")
				{
					cout << " Bad move, you REPEATED a Move! " << endl;
				}
				else if (opponent_board.player_grid[j][i] == "-")
				{
					//miss and update grid
					my_board.opponent_grid[j][i] = "*";
					cout << " Bad move, you MISSED!! " << endl;
				}
				else
				{
					cout << " Attack was SUCCESSFUL!!!" << endl;
					my_board.opponent_grid[j][i] = "H";
					opponent_board.player_grid[j][i] = "X";
					return true;
				}
			}
		}
	}
	//my_board.print_grid();
	return false;
}
/*
 * Function will iterate through all the ships passed in. It then will verify that that boat is not already sunk.
 *    If the boat is not sunk then it check it direction
 *    Then it gets is possition and verifies which boat was the one that was hit.
 *    It then will increment the hits for that specific boat
 */
bool isSunk(Player &p, vector <Battle_Ships> &boats, string attack)
{
	int row, col;
	bool boatSunk = false;

	for (auto & c : attack) c = toupper(c);

	// Get coordinate of hit
	for (int i = 1; i < column.size(); i++)
	{
		for (int j = 1; j < column.size(); j++)
		{
			if (attack == (column[j] + to_string(i)))
			{ // ex: a9
				row = j;
				col = i;
			}
			else if (attack == (to_string(i) + column[j]))
			{ // ex: 9a
				row = j;
				col = i;
			}
		}
	}

	// Iterate through all the boats passed in
	for (int i = 0; i < boats.size(); i++)
	{
		if (!boats[i].get_sunk())
		{
			if (boats[i].get_direction() == "H")
			{
				for (int j = 0; j < boats[i].get_ship_size(); j++)
				{
					if (boats[i].get_position_x() == row)  //Potential hit on this boat
					{
						// Verify if its a hit by checking if the 'col' lies within the boats position (pos_y + size)
						// Example of HIT pos_y = 2, ship_size = 3
						// col = 3
						// 1 + 3 = 4 (is 3 between 2,4) => TRUE

						// Example of MISS pos_y = 2, ship_size = 3
						// col = 5
						// 1 + 3 = 4 (is 5 between 2,4) => FALSE

						// Example of MISS pos_y = 2, ship_size = 3
						// col = 1
						// 1 + 3 = 4 (is 1 between 2,4) => FALSE
						if (boats[i].get_position_y() <= col && boats[i].get_position_y() + boats[i].get_ship_size() >= col)
						{
							boats[i].set_hit();
							if (boats[i].get_hit() == boats[i].get_ship_size())
							{
								boatSunk = true;
							}
							break;
						}
					}
				}
				for (int k = 0; k < boats[i].get_ship_size(); k++)
				{
					int col = boats[i].get_position_y();

					if (boatSunk)
					{
						p.opponent_grid[row][col + k] = "S";
						boats[i].set_sunk(true);
					}
				}
				if (boats[i].get_sunk())
				{
					return true;
				}

				boatSunk = false;
			}
			else
			{
				for (int j = 0; j < boats[i].get_ship_size(); j++)
				{
					if (boats[i].get_position_y() == col)  //Potential hit on this boat
					{
						if (boats[i].get_position_x() <= row && boats[i].get_position_x() + boats[i].get_ship_size() >= row)
						{
							boats[i].set_hit();
							if (boats[i].get_hit() == boats[i].get_ship_size())
							{
								boats[i].set_sunk(true);
								boatSunk = true;
							}
							break;
						}
					}
				}
				for (int k = 0; k < boats[i].get_ship_size(); k++)
				{
					int row = boats[i].get_position_x();

					if (boatSunk)
					{
						p.opponent_grid[row + k][col] = "S";
						boats[i].set_sunk(true);
					}
				}
				if (boats[i].get_sunk())
				{
					return true;
				}
			}
		}
		boatSunk = false;
	}

	return boatSunk;
}


/***************************** MAIN ***********************************/

int main(int argc, const char * argv[]) {

	string attack_in;
	string next;

	Player player;
	Player opponent;

	int playerSunkShips = 0;
	int opponentSunkShips = 0;
	int maxSunkShips = 5;

	player.set_player_name("Player 1");
	opponent.set_opponent_name("Player 2");

	Battle_Ships p_ship, o_ship;

	// create the objects
	Two_Sized_Ship p_two_size, o_two_size;
	Three_Sized_Ship p_three_size_A, o_three_size_A;
	Three_Sized_Ship p_three_size_B, o_three_size_B;
	Four_Sized_Ship p_four_size, o_four_size;
	Five_Sized_Ship p_five_size, o_five_size;

	// change the name of the 3-size ship
	p_three_size_B.set_name("Ship Three(2)");
	o_three_size_B.set_name("Ship Three(2)");

	// player ships
	p_ship.player_ships.push_back(p_two_size);
	p_ship.player_ships.push_back(p_three_size_A);
	p_ship.player_ships.push_back(p_three_size_B);
	p_ship.player_ships.push_back(p_four_size);
	p_ship.player_ships.push_back(p_five_size);

	// opponent ships
	o_ship.opponent_ships.push_back(o_two_size);
	o_ship.opponent_ships.push_back(o_three_size_A);
	o_ship.opponent_ships.push_back(o_three_size_B);
	o_ship.opponent_ships.push_back(o_four_size);
	o_ship.opponent_ships.push_back(o_five_size);

	for (int i = 0; i < p_ship.player_ships.size(); i++)
	{
		enter_direction(p_ship.player_ships[i]);
		enter_position(player, p_ship.player_ships[i]);
		update_grid(player, p_ship.player_ships[i]);
		player.print_grid();
	}

	cout << "Enter any key and hit [Enter] to clear board: " << endl;
	cin >> next;
	cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;

	for (int i = 0; i < o_ship.opponent_ships.size(); i++)
	{
		enter_direction(o_ship.opponent_ships[i]);
		enter_position(opponent, o_ship.opponent_ships[i]);
		update_grid(opponent, o_ship.opponent_ships[i]);
		opponent.print_grid();
	}

	cout << "Enter any key and hit [Enter] to clear board: " << endl;
	cin >> next;
	cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;

	while (!game_won)
	{
		player.print_grid();
		if (attack_in != "")
		{
			cout << "Opponents previous move: " + attack_in << endl;
		}
		cout << player.get_player_name() << " Enter attack position: " << endl;
		cin >> attack_in;

		if (validate_attack(attack_in, player, opponent))
		{
			// update which boats have been hit
			// Decrement the number of boats for the opponent if a boat has sunk
			if (isSunk(player, o_ship.opponent_ships, attack_in))
			{
				opponentSunkShips++;
			}
		}
		player.print_grid();
		// Check if Player has won the game
		if (maxSunkShips == opponentSunkShips)
		{
			cout << "GAME OVER " << opponent.get_opponent_name() << "WON!!!" << endl;
			cin >> next;
			exit(0);
		}


		cout << "Enter any key and hit [Enter] to clear board: " << endl;
		cin >> next;
		cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;

		opponent.print_grid();
		cout << "Opponents previous move: " + attack_in << endl;
		cout << opponent.get_opponent_name() << " Enter attack position: " << endl;
		cin >> attack_in;

		if (validate_attack(attack_in, opponent, player))
		{
			// update which boats have been hit
			// Decrement the number of boats for the player if a boat has sunk
			if (isSunk(opponent, p_ship.player_ships, attack_in))
			{
				playerSunkShips++;
			}
		}
		opponent.print_grid();
		// Check if Opponent has won the game
		if (maxSunkShips == playerSunkShips)
		{
			cout << "GAME OVER " << opponent.get_opponent_name() << "WON!!!" << endl;
			cin >> next;
			exit(0);
		}
		cout << "Enter any key and hit [Enter] to clear board: " << endl;
		cin >> next;
		cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nn\n\n\n\n\n\n\n" << endl;
	}
	return 0;
}
